#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Connor Grout'
SITENAME = 'Connor Grout Resume'
SITEURL = 'connor.grout.xyz'

PATH = 'content'

THEME = 'elegant'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en_US'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Sourcecode', 'https://gitlab.com/retsnom513/retsnom513.gitlab.io'),
         ('Pelican', 'https://getpelican.com/'),)

# Social widget
SOCIAL = (('LinkedIn', 'https://www.linkedin.com/in/connor-grout-22a751118'),
          ('GitLab', 'https://gitlab.com/retsnom513'),
          ('Phone: +1 (513) 720-9836', 'tel:+15137209836'),
          ('Email: connor@grout.xyz', 'mailto:connor@grout.xyz'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
